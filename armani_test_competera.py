import datetime
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from armani_test.items import ArmaniTestItem


def availability_filter(soldout_text):
    if soldout_text:
        return 'No'
    else:
        return 'Yes'


def category_filter(category_url):
    category = category_url.replace('http://www.armani.com/', '')
    index = category.index('_cod')
    category = category[:index]
    return category


def region_filter(url):
    if '/us/' in url:
        return 'USA'
    elif '/fr/' in url:
        return 'France'
    else:
        return 'Unknown region'


def description_fiter(description):
    description = description.replace('\xa0\r\n\t\t\t\t\t\t\t\t', ' ')
    # description = description.replace ('\r\n', ' ')
    return description


class ArmaniTest(CrawlSpider):

    name = 'armani_test_competera'
    allowed_domains = ['armani.com']
    user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0)\
     Gecko/20100101 Firefox/50.0'

    def __init__(self, country=None, *args, **kwargs):
        if country == 'all':
            self.start_urls = [
                'http://www.armani.com/fr', 'http://www.armani.com/us']
            self.rules = [
                Rule(LinkExtractor(allow=('\.html',)),
                     callback='parse_item'),
                Rule(LinkExtractor(allow=(r'armani.com/us', r'armani.com/fr')),)
            ]
        if country and country != 'all':
            self.start_urls = [
                'http://www.armani.com/{}'.format(country)]
            self.rules = [
                Rule(LinkExtractor(allow=('\.html',)),
                     callback='parse_item'),
                Rule(LinkExtractor(allow=(r'armani.com/{}'.format(country))),)
            ]
        super().__init__(*args, **kwargs)

    def parse_item(self, response):
        item = ArmaniTestItem()
        self.logger.info('Parsed url:  {}'.format(response.url))
        for title in response.css('.item.clearfix'):
            item['title'] = title.css('h2 ::text').extract_first(),
            item['price'] = title.css('.priceValue ::text').extract_first(),
            item['currency'] = title.css('.currency ::text').extract_first(),
            item['category'] = category_filter(response.url),
            item['code'] = title.css('.MFC ::text').extract_first(),
            item['availability'] = availability_filter(title.css(
                '.soldOutButton ::text').extract_first()),
            item['date_time'] =  \
                datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
            item['colors'] = title.css('.Colors a ::text').extract(),
            item['size'] = title.css('.SizeW a ::text').extract(),
            item['region'] = region_filter(response.url),
            item['description'] = description_fiter(title.css(
                '.descriptionContent ::text').extract_first()),
            return item