import json
import pandas as pd

ar = pd.read_csv('arma_new.csv')


total = len(ar.index)
total_us = len(ar.index[ar.region == 'USA'])
total_france = len(ar.index[ar.region == 'France'])

check_currency_france = ar.currency[(ar.region == 'France') &
                                    (ar.currency != '(None,)')]
check_currency_us = ar.currency[(ar.region == 'USA') &
                                (ar.currency != '(None,)')]


no_colors = ar.colors[ar.colors == '([],)'].count()
no_size = ar['size'][ar['size'] == '([],)'].count()

info = {

    "items": {
        "total": total,
        "total_france": total_france,
        "total_us": total_us},

    "correct_value_france": str(all(check_currency_france == "EUR")),
    "correct_value_us": str(all(check_currency_us == "$")),

    "percentage": {
        "colors": 100 - (no_colors / len(ar.index) * 100),
        "size": 100 - (no_size / len(ar.index) * 100),
        "description": (len(ar.description) / len(ar.index)) * 100},

}

with open('json_file_new.json', 'w') as f:
    json.dump(info, f, indent=2)
