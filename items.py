import scrapy


class ArmaniTestItem(scrapy.Item):

    title = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    category = scrapy.Field()
    code = scrapy.Field()
    availability = scrapy.Field()
    date_time = scrapy.Field()
    colors = scrapy.Field()
    size = scrapy.Field()
    region = scrapy.Field()
    description = scrapy.Field()
